// WideMargin. Simple fast bible software.
// Copyright (C) 2011  Daniel Hughes
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using WideMargin.MVCInterfaces;

namespace WideMargin.Database
{
	/// <summary>
	/// Represents a dayly reading which could be one or more chapters
	/// </summary>
	public class DaylyReading
	{
		/// <summary>
		/// Creates a dayly reading 
		/// </summary>
		/// <param name="firstVerse">first verse in reading</param>
		/// <param name="chapters">Human readable chapter range</param>
		public DaylyReading (IVerseIdentifier firstVerse, string chapters)
		{
			Chapters = chapters;
			FirstVerse = firstVerse;			
		}
		
		/// <summary>
		/// Human readable chapter range
		/// </summary>
		public string Chapters
		{
			get;
			private set;
		}
		
		/// <summary>
		/// First verse in dayly reading.
		/// </summary>
		public IVerseIdentifier FirstVerse
		{
			get;
			private set;
		}
	}
}

