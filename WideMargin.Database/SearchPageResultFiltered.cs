// WideMargin. Simple fast bible software.
// Copyright (C) 2012  Daniel Hughes
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using WideMargin.MVCInterfaces;

namespace WideMargin.Database
{
	public class SearchPageResultFiltered : SearchPageResult
	{
		public SearchPageResultFiltered (IEnumerable<IVerse> verses, IDictionary<string, int> matches, int pageSize, int pageNumber, string searchText, string book)
			:base(verses, matches, pageSize, pageNumber, searchText)
		{
			Book = book;
		}
		
		public string Book
		{
			private set;
			get;
		}
	}
}

