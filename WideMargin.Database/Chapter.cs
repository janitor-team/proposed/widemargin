// WideMargin. Simple fast bible software.
// Copyright (C) 2011  Daniel Hughes
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;

namespace WideMargin.Database
{
	public class Chapter
	{
		public Chapter(int number, int verseCount)
		{
			Number = number;
			VerseCount = verseCount;
		}
		
		public int Number
		{
			get;
			private set;
		}
		
		public int VerseCount
		{
			get;
			private set;
		}
	}
}

