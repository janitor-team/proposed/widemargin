// WideMargin. Simple fast bible software.
// Copyright (C) 2011  Daniel Hughes
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.IO;

namespace WideMargin.Database
{
	/// <summary>
	/// Helper methods for dealing the system paths and finding
	/// wide margin files
	/// </summary>
	public static class WideMarginPaths
	{		
		/// <summary>
		/// Gets the wide margin app data path
		/// </summary>
		public static string AppDataPath
		{
			get
			{
				string commonAppData = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);	
				return Path.Combine(commonAppData, "widemargin");
			}
		}
		
		/// <summary>
		/// Get the path to the specified wide margin application data file
		/// Check the current directory first (might be running from IDE)
		/// Then checks the wide margin app data path
		/// </summary>
		/// <param name="fileName">File to get path of</param>
		/// <returns>path to file.</returns>
		public static string GetApplicationDataFile(string fileName)
		{
			string path = Path.Combine("..","..","..","TextFiles",fileName);
			if(!File.Exists(path))
			{
				path = Path.Combine(WideMarginPaths.AppDataPath, fileName);
				if(!File.Exists(path))
				{
					throw new FileNotFoundException("Could not find the file.", path);
				}
			}
			return path;
		}
	}
}

