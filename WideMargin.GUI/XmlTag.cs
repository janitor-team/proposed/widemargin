// WideMargin. Simple fast bible software.
// Copyright (C) 2011  Daniel Hughes
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;

namespace WideMargin.GUI
{
	/// <summary>
	/// Represents and xml tag with a name and atttributes.
	/// </summary>
	public class XmlTag
	{
		/// <summary>
		/// Constructs and XmlTag from the raw tag string without the '<' '>'
		/// </summary>
		/// <param name="tag">Raw tag string</param>
		public XmlTag (string tag)
		{
			Attributes = new Dictionary<string, string>();
			int firstSpace = tag.IndexOf(' ');
			if(firstSpace == -1)
			{
				//no attributes
				Name = tag;
				return;
			}
			Name = tag.Substring(0, firstSpace);
			IEnumerable<string> parts = SplitAttrubutes(tag.Substring(firstSpace + 1, tag.Length - firstSpace - 1));
			foreach(string attribute in parts)
			{
				string[] attributeParts = attribute.Split('=');
				string attributeValue = attributeParts[1];
				if(attributeValue[0] == '\'')
				{
					attributeValue = attributeValue.Substring(1, attributeValue.Length - 2); 
				}
				Attributes.Add(attributeParts[0], attributeValue);
			}
		}
		
		/// <summary>
		/// Spits up the attrubute part of the tag
		/// </summary>
		/// <param name="attributes">space separated attributes</param>
		/// <returns>Attributes</returns>
		private IEnumerable<string> SplitAttrubutes(string attributes)
		{
			List<string> attributesList = new List<string>();
			int index = 0;
			while(index < attributes.Length - 1)
			{
				string attribute = ReadAttribute(attributes, index);
				index += attribute.Length;
				attributesList.Add(attribute);
			}
			return attributesList;
		}
		
		/// <summary>
		/// Reads the attribute starting at the specified index.
		/// </summary>
		/// <param name="attributes">string of all attributes</param>
		/// <param name="startIndex">index attribute starts at</param>
		/// <returns>attribute</returns>
		private string ReadAttribute(string attributes, int startIndex)
		{
			int equalsIndex = attributes.IndexOf('=',startIndex);
			int endIndex = 0;
			if(attributes[equalsIndex + 1] == '\'')
			{
				//the attribute value is in quotes
				endIndex = attributes.IndexOf('\'', equalsIndex + 2);
			}
			else
			{
				endIndex = attributes.IndexOf(' ', equalsIndex + 1);	
			}
			
			return attributes.Substring(startIndex, endIndex - startIndex + 1);
		}		
		
		/// <summary>
		/// The name of the tag
		/// </summary>
		public string Name
		{
			get;
			private set;
		}
		
		/// <summary>
		/// The attributes of the tag
		/// </summary>
		public IDictionary<string,string> Attributes
		{
			get;
			private set;
		}
		
		/// <summary>
		/// Gets or set the starting index for this tag
		/// </summary>
		public int StartingIndex
		{
			get;
			set;
		}
		
		/// <summary>
		/// Gets or set the ending index;
		/// </summary>
		public int EndIndex
		{
			get;
			set;
		}
	}
}

