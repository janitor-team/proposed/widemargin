// WideMargin. Simple fast bible software.
// Copyright (C) 2011  Daniel Hughes
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using Gtk;
using WideMargin.MVCInterfaces;
using WideMargin.Utilities;

namespace WideMargin.GUI
{
	/// <summary>
	/// Tag for a verse link
	/// </summary>
	public class LinkTag<Target> : TextTag
	{
		private EventLocker _eventLocker = new EventLocker();
		private EventHandler<LinkClickedEventArgs<Target>> _linkClicked;
		
		/// <summary>
		/// Creates a link tag
		/// </summary>
		/// <param name="name">all tags must have unique names within the tab table</param>
		/// <param name="target">The verse to navigate to when clicking on the link</param>
		public LinkTag (string name, Target target) 
			: base(name)
		{
			Foreground = "Blue";
			LinkTarget = target;
		}
		
		/// <summary>
		/// Gets the verse target for this link.
		/// </summary>
		public Target LinkTarget
		{
			get;
			private set;
		}
		
		public event EventHandler<LinkClickedEventArgs<Target>> LinkClicked
		{
			add
			{
				_eventLocker.Add(ref _linkClicked, value);	
			}
			remove
			{
				_eventLocker.Remove(ref _linkClicked, value);	
			}
		}
		
		protected override bool OnTextEvent (GLib.Object  sender,
		                                     Gdk.Event    textEvent,
		                                     Gtk.TextIter iter)
		{
			if(textEvent.Type == Gdk.EventType.ButtonPress) 
			{
				Gdk.EventButton mouseButton = new Gdk.EventButton (textEvent.Handle);

				_linkClicked.Fire(this, 
				                  new LinkClickedEventArgs<Target>(LinkTarget, (int)mouseButton.Button));
				return true;
			}
			return false;
		}
	}
}

