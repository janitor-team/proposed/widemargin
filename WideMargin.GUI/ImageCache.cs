// WideMargin. Simple fast bible software.
// Copyright (C) 2011  Daniel Hughes
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using Gdk;
using System.IO;

namespace WideMargin.GUI
{
	/// <summary>
	/// Caches images so we only have them in memory once.
	/// </summary>
	public class ImageCache
	{
		private static ImageCache _instants;
		private Pixbuf _closeImage;
		private Pixbuf _backArrow;
		private Pixbuf _forwardArrow;
		private Pixbuf _mainIconSmall;
		private Pixbuf _settings;
		private Pixbuf _longBackArrow;
		private Pixbuf _longForwardArrow;
		
		/// <summary>
		/// Constructor
		/// </summary>
		private ImageCache ()
		{
		}
		
		/// <summary>
		/// gets the image cache instance
		/// </summary>
		/// <returns>
		/// ImageCache
		/// </returns>
		public static ImageCache Get()
		{
			if(_instants == null)
			{
				_instants = new ImageCache();
			}
			return _instants;
		}
		
		/// <summary>
		/// Close image 'x'
		/// </summary>
		public Pixbuf CloseImage
		{
			get
			{
				if(_closeImage == null)
				{
					_closeImage = GetImage("CloseImage.png");
				}
				return _closeImage;
			}
		}
		
		/// <summary>
		/// Foward arrow
		/// </summary>
		public Pixbuf ForwardArrow
		{
			get
			{
				if(_forwardArrow == null)
				{
					_forwardArrow = GetImage("arrow.png");
				}
				return _forwardArrow;
			}
		}
		
		/// <summary>
		/// Back arrow
		/// </summary>
		public Pixbuf BackArrow
		{
			get
			{
				if(_backArrow == null)
				{
					_backArrow = GetImage("barrow.png");
				}
				return _backArrow;
			}
		}
		
		/// <summary>
		/// Long Foward arrow
		/// </summary>
		public Pixbuf LongForwardArrow
		{
			get
			{
				if(_longForwardArrow == null)
				{
					_longForwardArrow = GetImage("longForwardArrow.png");
				}
				return _longForwardArrow;
			}
		}
		
		/// <summary>
		/// Long Back arrow
		/// </summary>
		public Pixbuf LongBackArrow
		{
			get
			{
				if(_longBackArrow == null)
				{
					_longBackArrow = GetImage("longBackArrow.png");
				}
				return _longBackArrow;
			}
		}
		
		/// <summary>
		/// Settings Icon (Cog)
		/// </summary>
		public Pixbuf Settings
		{
			get
			{
				if(_settings == null)
				{
					_settings = GetImage("Settings.png");
				}
				return _settings;
			}
		}
		
		/// <summary>
		/// Main Icon 16x16
		/// </summary>
		public Pixbuf MainIconSmall
		{
			get
			{
				if(_mainIconSmall == null)
				{
					_mainIconSmall = GetImage("widemargin.png");
				}
				return _mainIconSmall;
			}
		}
		
		/// <summary>
		/// Gets the image with the specified name.
		/// Check the current directory then common app data
		/// </summary>
		/// <param name="filename">name of file</param>
		/// <returns>image</returns>
		private Pixbuf GetImage(string filename)
		{
			string path = Path.Combine("..","..", "..", "Images", filename);
			if(!File.Exists(path))
			{
				string commonAppData = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);
				string wideMarginCommonAppData = Path.Combine(commonAppData, "widemargin");
				path = Path.Combine(wideMarginCommonAppData, filename);
			}
			return new Gdk.Pixbuf(path);
		}			
	}
}

