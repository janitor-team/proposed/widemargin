// WideMargin. Simple fast bible software.
// Copyright (C) 2011  Daniel Hughes
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using Gtk;
using WideMargin.MVCInterfaces;
using WideMargin.Utilities;

namespace WideMargin.GUI
{
	/// <summary>
	/// Class for displaying generic text with verse links.
	/// </summary>
	[System.ComponentModel.ToolboxItem(true)]
	public partial class GenericView : Gtk.Bin, IGenericView
	{
		private EventLocker _eventLocker = new EventLocker();
		private EventHandler<LinkClickedEventArgs<IVerseIdentifier>> _verseLinkClicked;
		private EventHandler<LinkClickedEventArgs<string>> _actionLinkClicked;
		private RichTextView _richTextView;
		
		/// <summary>
		/// Default constructor
		/// </summary>
		public GenericView ()
		{
			this.Build ();
			_richTextView = new RichTextView();
			_richTextView.VerseLinkClicked += HandleTagLinkClicked;
			_richTextView.ActionLinkClicked += HandleActionLinkClicked;
			Add(_richTextView);
		}

		/// <summary>
		/// An action link has been clicked, pass on the event
		/// </summary>
		private void HandleActionLinkClicked (object sender, LinkClickedEventArgs<string> e)
		{
			_actionLinkClicked.Fire(this, e);
		}
		
		
		/// <summary>
		/// Text including markup
		/// </summary>
		public string Text
		{
			set
			{
				this.Invoke (() => 
				{
					_richTextView.Text = value;
				});
			}
		}

		/// <summary>
		/// A verse link has been cliked, Passes the event up to the controller.
		/// </summary>
		private void HandleTagLinkClicked (object sender, LinkClickedEventArgs<IVerseIdentifier> e)
		{
			_verseLinkClicked.Fire(this, e);
		}
		
		/// <summary>
		/// Occurs when a verse link is clicked.
		/// </summary>
		public event EventHandler<LinkClickedEventArgs<IVerseIdentifier>> VerseLinkClicked
		{
			add
			{
				_eventLocker.Add(ref _verseLinkClicked, value);
			}
			remove
			{
				_eventLocker.Remove(ref _verseLinkClicked, value);
			}
		}
		
		/// <summary>
		/// Occurs when a action link is clicked.
		/// </summary>
		public event EventHandler<LinkClickedEventArgs<string>> ActionLinkClicked
		{
			add
			{
				_eventLocker.Add(ref _actionLinkClicked, value);
			}
			remove
			{
				_eventLocker.Remove(ref _actionLinkClicked, value);
			}
		}
	}
}

