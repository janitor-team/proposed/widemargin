// WideMargin. Simple fast bible software.
// Copyright (C) 2011  Daniel Hughes
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using Gtk;
using System.Linq;
using System.Data.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using Mono.Data.Sqlite;
using System.Threading;
using WideMargin.GUI;
using WideMargin.MVCInterfaces;
using WideMargin.Utilities;

public partial class MainWindow : Gtk.Window
{
	private Notebook _noteBook;
	private List<SearchPage> _tabPages;
	private EventHandler<EventArgs> _newTabRequest;
	private EventLocker _eventLock = new EventLocker();
	
	public MainWindow () : base(Gtk.WindowType.Toplevel)
	{
		Build ();
		
		this.HeightRequest = 672;
		this.WidthRequest = 800;
		
		_tabPages = new List<SearchPage>();
		_noteBook = new Notebook();
		_noteBook.Scrollable = true;
		_noteBook.AppendPage(new Label(), new Label("+"));
		_noteBook.SwitchPage += HandleNoteBookSwitchPage;
		Add(_noteBook);
		_noteBook.ShowAll();
		Icon = ImageCache.Get().MainIconSmall;
		
	}

	private void HandleNoteBookSwitchPage (object o, SwitchPageArgs args)
	{
		if(args.PageNum == _noteBook.NPages -1)
		{
			_newTabRequest.Fire(this, new EventArgs());
		}
		else
		{
			_tabPages[(int)args.PageNum].FocusBibleBar();
		}
	}

	public event EventHandler<EventArgs> NewTabRequest
	{
		add
		{
			_eventLock.Add(ref _newTabRequest, value);
		}
		remove
		{
			_eventLock.Remove(ref _newTabRequest, value);
		}
	}
	
	internal SearchPage AddTab(bool show)
	{
		var searchPage = new SearchPage();
		searchPage.Closing += OnTabClosing;
		_tabPages.Add(searchPage);
		int pageIndex = _noteBook.InsertPage(searchPage, searchPage.CreateTabTitle(), _noteBook.Children.Length - 1);
		_noteBook.ShowAll();
		
		if(show)
		{
			ActivateTab(pageIndex);
			searchPage.FocusBibleBar();
		}
		return searchPage;
	}
	
	private void ActivateTab(int pageIndex)
	{
		this.Invoke(() =>
		{
			_noteBook.CurrentPage = pageIndex;
		});
	}
	
	private void OnTabClosing(object sender, EventArgs args)
	{
		Widget page = (Widget)sender;
		int pageIndex = _noteBook.PageNum(page);
		int numberOfPages = _noteBook.NPages;
		//if second to last page removing it will activate add page
		//causing a new page to be added.
		if(pageIndex == _noteBook.NPages - 2)
		{	
			if(numberOfPages - 3 >= 0)
			{
				_noteBook.CurrentPage = _noteBook.NPages - 3;
			}
		}
		
		
		_noteBook.Remove((Gtk.Widget)sender);
		numberOfPages--;
		
		if(numberOfPages == 1)
		{
			Destroy();
			OnDestroyed();
			Application.Quit();
		}
	}                            

	protected void OnDeleteEvent (object sender, DeleteEventArgs a)
	{
		Application.Quit ();
		a.RetVal = true;
	}
}

