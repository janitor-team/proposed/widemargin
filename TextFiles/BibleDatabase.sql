BEGIN TRANSACTION;
CREATE VIRTUAL TABLE Verse USING fts4(Book TEXT, Chapter NUMERIC, VerseNumber NUMERIC, Contents TEXT);
CREATE TABLE ReadingPlanner (Day NUMERIC, Month NUMERIC, FirstPortion TEXT, SecondPortion Text, ThirdPortion Text);
CREATE TABLE Settings (Version NUMERIC);
INSERT INTO Settings VALUES (1);
COMMIT;