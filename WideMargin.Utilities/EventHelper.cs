// WideMargin. Simple fast bible software.
// Copyright (C) 2011  Daniel Hughes
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
namespace WideMargin.Utilities
{
	/// <summary>
	/// Extension methods for EventHandlers
	/// </summary>
	public static class EventHelper
	{
		/// <summary>
		/// Fires an event,
		/// Takes care of checking for null first.
		/// </summary>
		/// <param name="handler">
		/// Event to fire
		/// </param>
		/// <param name="sender">
		/// object firing the event
		/// </param>
		/// <param name="args">
		/// EventArgs for the event
		/// </param>
		public static void Fire<T>(this EventHandler<T> handler, object sender, T args) where T : EventArgs
		{
			if(handler == null)
			{
				return;	
			}
			handler.Invoke(sender, args);
		}
	}
}

