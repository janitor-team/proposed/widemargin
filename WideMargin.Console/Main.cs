// WideMargin. Simple fast bible software.
// Copyright (C) 2011  Daniel Hughes
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Collections;
using WideMargin.Database;
using System.Diagnostics;

namespace WideMargin.ConsoleUI 
{
	class MainClass
	{
	
		public static void Main (string[] args)
		{
			GenerateLinqToSqlMappings();
			OSISImporter.Import(@"/home/daniel/Downloads/kjvosis/kjv.rawtxt", @"KJV.db");
		}
		
		public static void GenerateLinqToSqlMappings()
		{
			//create a database
			OSISImporter.CreateDatabase("temp.db");
			
			string sqlMetalArgs = "/conn:\"Data Source=temp.db\" /provider:Sqlite --code:BibleDatabase.cs --namespace=\"WideMargin.Database\" -d BibleDatabase";
			
		
			var startInfo = new ProcessStartInfo("sqlmetal", sqlMetalArgs);
			using(Process process = Process.Start(startInfo))
			{
				process.WaitForExit();	
				if(process.ExitCode != 0)
				{
					throw new InvalidOperationException("sqlMetal failed");	
				}
			}
		}

	}
}