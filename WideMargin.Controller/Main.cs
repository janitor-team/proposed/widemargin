// WideMargin. Simple fast bible software.
// Copyright (C) 2011  Daniel Hughes
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using WideMargin.MVCInterfaces;
using WideMargin.GUI;
using WideMargin.Database;
using System.IO;

namespace WideMargin.Controller
{
	/// <summary>
	/// Main class exists for no other reason then hosting the main 
	/// method
	/// </summary>
	class MainClass
	{
		/// <summary>
		/// On windows GTK# has just been installed and the computer has not been restarted then the
		/// path will not be set yet.
		/// </summary>
		public static void DoPathHackForWindows ()
		{
			if (Environment.OSVersion.Platform == PlatformID.Win32NT) 
			{
				if(!Environment.GetEnvironmentVariable("PATH").Contains ("GtkSharp"))
				{
					string programFiles;
					if(Environment.Is64BitOperatingSystem)
					{
						programFiles = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86);
					}
					else
					{
						programFiles = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles);
					}
					Environment.CurrentDirectory =  Path.Combine(programFiles, "GtkSharp","2.12","bin");
				}
			}
		}

		/// <summary>
		/// Entry point to program
		/// </summary>
		/// <param name="args">
		/// no command line arguments as this is a graphical program.
		/// </param>
		public static void Main (string[] args)
		{
			DoPathHackForWindows();

			string appData = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
			string wideMarginAppDataPath = Path.Combine(appData, "widemargin");
			if(!Directory.Exists(wideMarginAppDataPath))
			{
				Directory.CreateDirectory(wideMarginAppDataPath);
			}
			string kjvPath = Path.Combine(wideMarginAppDataPath, "KJV.db");
			using(IWideMarginView view = new GTKWideMarginView())
			using(BibleDatabase bible = new BibleDatabase(kjvPath))
			using(WideMarginController controller = new WideMarginController(view, bible))
			{
				controller.Run();	
			}
		}
	}
}

