// WideMargin. Simple fast bible software.
// Copyright (C) 2011  Daniel Hughes
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using WideMargin.Database;
using WideMargin.MVCInterfaces;

namespace WideMargin.Controller
{
	/// <summary>
	/// Top level controller for a collection of pages or tabs.
	/// </summary>
	public class WideMarginController : IDisposable
	{
		private IWideMarginView _wideMarginView;
		private List<BibleController> _pages = new List<BibleController>();
		private BibleDatabase _bible;
		private BibleBarDecoder _bibleBarDecoder;
		private QueryQueue _queryQueue;
		
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="wideMarginView">view of a collection of pages or tabs</param>
		/// <param name="bible">database providing access to the bible.</param>
		public WideMarginController (IWideMarginView wideMarginView, BibleDatabase bible)
		{
			_wideMarginView = wideMarginView;
			_wideMarginView.NewTabRequest += HandleWideMarginViewNewTabRequest;
			
			_bible = bible;
			_bibleBarDecoder = new BibleBarDecoder();
			_queryQueue = new QueryQueue();
		}

		/// <summary>
		/// Starts Wide Margin with a default dayly reading page. 
		/// </summary>
		public void Run()
		{
			BibleController page = AddNewPage(true);	
			page.ShowDaylyReading();
			_wideMarginView.Run();
		}
		
		/// <summary>
		/// View is requesting a new tab,
		/// creates a new tab showing the reading planner.
		/// </summary>
		public void HandleWideMarginViewNewTabRequest (object sender, EventArgs e)
		{
			BibleController page = AddNewPage(true);	
			page.ShowDaylyReading();
		}
		
		/// <summary>
		/// Add a new page
		/// </summary>
		/// <param name="show">true to show the new page now</param>
		/// <returns>Controller for new page</returns>
		private BibleController AddNewPage(bool show)
		{
			var page = new BibleController(_wideMarginView.CreateBibleView(show), _queryQueue, _bibleBarDecoder, _bible);
			page.NewPassagePageRequest += HandlePageNewPassagePageRequest;
			page.NewBooksPageRequest += HandleNewBooksPageRequest;
			_pages.Add(page);
			return page;
		}
		
		/// <summary>
		/// Handles the new books page request.
		/// </summary>
		/// <param name='sender'>Sender.</param>
		/// <param name='e'>
		/// event args containing the book to show or Browse if 
		/// requesting the books list.
		/// </param>
		private void HandleNewBooksPageRequest (object sender, LinkClickedEventArgs<string> e)
		{
			BibleController page = AddNewPage(false);
			if(e.LinkTarget == "Browse")
			{
				page.ShowBooksList();
				return;
			}
			page.ShowBooksList(e.LinkTarget);
		}

		/// <summary>
		/// View is requesting a new page showing a passage
		/// </summary>
		private void HandlePageNewPassagePageRequest (object sender, VerseEventArgs e)
		{
			BibleController page = AddNewPage(false);
			page.ShowPassage(e.Verse);
		}
		
		/// <summary>
		/// Clean up resources.
		/// </summary>
		/// <param name="disposing">true if disposing managed resources</param>
		protected virtual void Dispose(bool disposing)
		{
			if(disposing)
			{
				if(_queryQueue != null)
				{
					_queryQueue.Dispose();
					_queryQueue = null;
				}
			}
		}
		
		/// <summary>
		/// Clean up
		/// </summary>
		public void Dispose()
		{
			GC.SuppressFinalize(this);
			Dispose(true);
		}
	}
}

