// WideMargin. Simple fast bible software.
// Copyright (C) 2011  Daniel Hughes
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using WideMargin.Database;

namespace WideMargin.Controller
{
	/// <summary>
	/// Event args for requesting a new page of search results.
	/// </summary>
	public class RequestNewResultsPageEventArgs : EventArgs
	{
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="result">results to get page of</param>
		/// <param name="page">page number to get</param>
		public RequestNewResultsPageEventArgs (SearchPageResult result, int page)
		{
			PreviousResult = result;
			Page = page;
		}
		
		/// <summary>
		/// Search result to get a new page of
		/// </summary>
		public SearchPageResult PreviousResult
		{
			get;
			private set;
		}
		
		/// <summary>
		/// Number of page to get
		/// </summary>
		public int Page
		{
			get;
			private set;
		}
	}
}

