// WideMargin. Simple fast bible software.
// Copyright (C) 2011  Daniel Hughes
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using WideMargin.MVCInterfaces;
using WideMargin.Database;
using System.Collections.Generic;
using System.Text;
using WideMargin.Utilities;

namespace WideMargin.Controller
{
	/// <summary>
	/// Controller for the books view, used for mouse friendly lookup of passages.
	/// </summary>
	public class BooksController
	{
		private Action<Action> _changeView;
		private IBibleView _bibleView;
		private IFilteredView _booksView;
		
		/// <summary>
		/// Initializes a new instance of the <see cref="WideMargin.Controller.BooksController"/> class.
		/// </summary>
		/// <param name='changeView'>Changes the view in a way which means the history works</param>
		/// <param name='bibleView'>Access to a tab in widemargin.</param>
		public BooksController (Action<Action> changeView, IBibleView bibleView)
		{
			_changeView = changeView;
			_bibleView = bibleView;
			_booksView = _bibleView.BooksView;
			
			var bookList = BookList.Get();
			_booksView = _bibleView.BooksView;
			_booksView.ActionLinkClicked += HandleBooksViewActionLinkClicked;
			_booksView.VerseLinkClicked += HandleVerseLinkClicked;
			
			_booksView.Filters = new Filter[]
			{
				new Filter("All", () => ShowBookList(bookList.AllBookNames())),
				new Filter("Old Testiment", () => ShowBookList(bookList.GetCatagory("Old Testiment"))),
				new Filter("New Testiment", () => ShowBookList(bookList.GetCatagory("New Testiment"))),
				new Filter("Torah", () => ShowBookList(bookList.GetCatagory("Torah"))),
				new Filter("Prophecy", () => ShowBookList(bookList.GetCatagory("Prophecy"))),
				new Filter("Poetry", () => ShowBookList(bookList.GetCatagory("Poetry"))),
				new Filter("Gospels", () => ShowBookList(bookList.GetCatagory("Gospels"))),
				new Filter("Letters", () => ShowBookList(bookList.GetCatagory("Letters"))),
				
			};
		}
		
		/// <summary>
		/// A verse link has been clicked. Pass it on to the bible controller
		/// </summary>
		private void HandleVerseLinkClicked (object sender, LinkClickedEventArgs<IVerseIdentifier> e)
		{
			if(VerseLinkClicked != null)
			{
				VerseLinkClicked(this, e);	
			}
		}
		
		/// <summary>
		/// Occurs when a verse link is clicked.
		/// </summary>
		public event EventHandler<LinkClickedEventArgs<IVerseIdentifier>> VerseLinkClicked;
		
		/// <summary>
		/// Occurs when a book link is clicked.
		/// </summary>
		public event EventHandler<LinkClickedEventArgs<string>> BookLinkClicked;
			
		/// <summary>
		/// Handles the books view action link clicked. 
		/// Which occurs when a book is clicked.
		/// Generates a list of chapters with verse links.
		/// </summary>
		private void HandleBooksViewActionLinkClicked (object sender, LinkClickedEventArgs<string> e)
		{
			if(e.Button == 1)
			{
				ShowChapters(e.LinkTarget);
				return;
			}
			if(e.Button == 2)
			{
				BookLinkClicked.Fire(this, e);
				return;
			}
		}
		
		/// <summary>
		/// Shows the chapters for a book
		/// </summary>
		/// <param name='book'>book</param>
		public void ShowChapters(string book)
		{
			StringBuilder chaptersBuilder = new StringBuilder();
			chaptersBuilder.Append("<Text><Center><Size value='14'>");
			
			int chapterCounter = BookList.Get().GetBook(book).ChapterCount;
			for(int chapter = 1; chapter <= chapterCounter; chapter++)
			{
				chaptersBuilder.AppendFormat(" <VerseLink target='{0} {1}:1'>{1}</VerseLink> {2}",
						                     book,
						                     chapter,
						                     Environment.NewLine);
			}
			chaptersBuilder.Append("</Size></Center></Text>");
			_changeView(() =>
			{
				_booksView.Text = chaptersBuilder.ToString();
				_bibleView.ShowBooksView();
				_bibleView.ChangeTitle("Browse");
			});
		}
		
		/// <summary>
		/// Shows a list of books.
		/// </summary>
		/// <param name='bookList'>books to show</param>
		public void ShowBookList(IEnumerable<string> bookList)
	    {
			var books = new StringBuilder();
			books.Append("<Text><Center><Size value='14'>");
			
			foreach(var book in bookList)
			{
				books.AppendFormat(" <ActionLink target='{0}'>{0}</ActionLink> {1}", book, Environment.NewLine);
			}
			books.Append("</Size></Center></Text>");
			_changeView(() =>
			{
				_booksView.Text = books.ToString();
				_bibleView.ShowBooksView();
				_bibleView.ChangeTitle("Browse");
			});
		}
	}
}

