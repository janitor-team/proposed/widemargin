// WideMargin. Simple fast bible software.
// Copyright (C) 2011  Daniel Hughes
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using WideMargin.Database;
using NUnit.Framework;
	
namespace WideMargin.Database.Tests
{
	[TestFixture]
	public class BibleBarDecoderTests
	{
		[Test]
		public void DecodeBookOnly()
		{
			BibleBarDecoder decoder = new BibleBarDecoder();
			BibleBarResult result = decoder.Decode("Genesis");
			Assert.That(result is PassageResult);
			PassageResult passageResult = (PassageResult) result;
			Assert.That(passageResult.Book == "Genesis");
			Assert.That(passageResult.Chapter == 1);
			Assert.That(passageResult.Verse == 1);
		}
		
		[Test]
		public void DecodeBookAndChapterOnly()
		{
			BibleBarDecoder decoder = new BibleBarDecoder();
			BibleBarResult result = decoder.Decode("Mark 10");
			Assert.That(result is PassageResult);
			PassageResult passageResult = (PassageResult) result;
			Assert.That(passageResult.Book == "Mark");
			Assert.That(passageResult.Chapter == 10);
			Assert.That(passageResult.Verse == 1);
		}
		
		[Test]
		public void DecodeFullLocation()
		{
			BibleBarDecoder decoder = new BibleBarDecoder();
			BibleBarResult result = decoder.Decode("Jonah 3:10");
			Assert.That(result is PassageResult);
			PassageResult passageResult = (PassageResult) result;
			Assert.That(passageResult.Book == "Jonah");
			Assert.That(passageResult.Chapter == 3);
			Assert.That(passageResult.Verse == 10);
		}
		
		[Test]
		public void DecodeWithBookNumber()
		{
			BibleBarDecoder decoder = new BibleBarDecoder();
			BibleBarResult result = decoder.Decode("2 Kings 3:15");
			Assert.That(result is PassageResult);
			PassageResult passageResult = (PassageResult) result;
			Assert.That(passageResult.Book == "2 Kings");
			Assert.That(passageResult.Chapter == 3);
			Assert.That(passageResult.Verse == 15);
		}
		
		[Test]
		public void DecodeMultiWordBook()
		{
			BibleBarDecoder decoder = new BibleBarDecoder();
			BibleBarResult result = decoder.Decode("Song of Solomon 3:15");
			Assert.That(result is PassageResult);
			PassageResult passageResult = (PassageResult) result;
			Assert.That(passageResult.Book == "Song of Solomon");
			Assert.That(passageResult.Chapter == 3);
			Assert.That(passageResult.Verse == 15);
		}
	}
}

